<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<div id="primary" class="content-area wishlist-wrapper">
    <main id="main" class="site-main" role="main">
		<div class="container">
			<div class="row">
                <div class="col-xs-12">

                    <div class="tps-section">
                        <h1 class="text-center">
                            <?php _e('My Wishlist','tps-wishlist');?>
                        </h1>
                        <div class="tps-my-account-avatar"><?php echo get_avatar( 0, 150 );?></div>
                    </div>

                    <div class="tps-wishlist-grid tps-section">

                        <p class="tps-wishlist-empty text-center">
                            <?php _e('Add your favorites products in your wishlist and share it with your friends.','tps-wishlist');?>
                        </p>
                        <p class="text-center">
                            <a class="btn btn-primary" href="<?php echo home_url(TPS_PROFILE_PAGE); ?>"><?php _e('Sign in το view your Wishlist', 'tps'); ?></a>
                        </p>
                    </div>

                </div>
            </div><!-- row -->
		</div><!-- container -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php

get_footer();