<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$user_id = get_query_var( 'wishlist' , false );

$wishlist_permalink = urlencode( tps_wishlist_permalink ( $user_id ) );

?>

<h4><?php _e('Share', 'tps-wishlist');?></h4>

<div class="tps-product-share">
    <a href="//www.facebook.com/sharer.php?u=<?php echo $wishlist_permalink;?>" class="tps-btn-social tps-btn-facebook" target="_blank" title="Share it on Facebook">
	    <i class="fa fa-facebook fa-fw fa-lg"></i>
    </a>
    <a href="//twitter.com/home?status=<?php echo $wishlist_permalink;?>" class="tps-btn-social tps-btn-twitter" target="_blank" title="Share it on Twitter">
	    <i class="fa fa-twitter fa-fw fa-lg"></i>
    </a>
    <a href="//plus.google.com/share?url=<?php echo $wishlist_permalink;?>" class="tps-btn-social tps-btn-google-plus" target="_blank" title="Share it on Google Plus">
        <i class="fa fa-google-plus fa-fw fa-lg"></i>
    </a>
    <a href="mailto:?subject=My Wishlist on The Parents Shop&amp;body=<?php echo $wishlist_permalink;?>" class="tps-btn-social tps-btn-email" title="Share it with Email">
	    <i class="fa fa-envelope-o fa-fw fa-lg"></i>
    </a>
</div>