<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wp_query;

//Check if user has products in their wishlist	
$user = get_user_by( 'email', urldecode ( $_GET['user_email'] )  );

$wishlist_products = tps_wishlist_products( $user->ID );

if ( empty ( $wishlist_products ) ) {
    
	$wp_query->set_404();
	status_header( 404 );
	nocache_headers();
	include( get_query_template( '404' ) );
	exit(); 
}

$wishlist_permalink = tps_wishlist_permalink( $user->ID );

$wishlist_greeting = ( !empty( $user->first_name ) ) ? __('Hey ').$user->first_name : __('Hey ');

$query_args = array(
	'posts_per_page' => TPS_NEWSLETTER_EMAILS_LOOP_PRODUCTS_NO,
	'orderby'        => 'date',
	'order'          => 'desc',
	'no_found_rows'  => false,
	'post_status'    => 'publish',
	'post_type'      => 'product',
	'operator' => 'IN',
	'post__in'       =>  $wishlist_products 
);

$email_wp_query = new WP_Query( $query_args );

$title_html = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/title.html');
$text_html = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/text.html');
$button_html = file_get_contents(TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH.'/partials/html/button.html');

?>

<?php 

do_action ('tps-newletter-email-html-start');

$button_args = array(
	tps_utm ( $wishlist_permalink , [ 'utm_content' => 'wishlist_cta_button' ] ), // 1: href
	'View & Share your Wishlist now', // 2: text
);

$title_args = array(
	$wishlist_greeting.', this is your wishlist!', // 1: title
);

echo vsprintf( $title_html , $title_args );

$text_args = array(
	'Sharing your wishlist with your friends is a great way to let them know about the products you love most.', // 1: title
);

echo vsprintf( $text_html , $text_args );

echo vsprintf( $button_html , $button_args );

include TPS_NEWSLETTER_EMAIL_TEMPLATES_PATH . '/partials/loop.php';

echo vsprintf( $button_html , $button_args );

do_action ('tps-newletter-email-html-end'); 

?>