<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<p><?php _e('Publish your wishlist and share it with your friends.','tps-wishlist');?></p>
<a href="#" class="tps-wishlist-ajax tps-wishlist-btn publish">
    <span><?php _e('Publish my Wishlist','tps-wishlist');?></span>
    <i class="tps-wishlist-loading hide fa fa-refresh fa-spin fa-fw"></i>
</a>