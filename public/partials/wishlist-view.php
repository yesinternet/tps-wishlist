<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tps
 */

$user_id = get_query_var( 'wishlist' , false );

$user = get_user_by( 'id', $user_id  );

$wishlist_is_public = get_user_meta( $user_id, '_tps_wishlist_public', true );

$wishlist_products = tps_wishlist_products( $user_id );

get_header(); ?>

<div id="primary" class="content-area wishlist-wrapper">
    <main id="main" class="site-main" role="main">
		<div class="container">
			<div class="row">
                <div class="col-xs-12">

                    <div class="tps-section">
                        <h1 class="text-center">
                            <?php if ( !empty ($user->first_name)  || !empty ($user->last_name) ) :?>
                            <p><?php echo $user->first_name . ' ' . $user->last_name;?></p>
                            <?php endif;?>

                            <p><?php _e('My Wishlist','tps-wishlist');?></p>

                        </h1>
                        <div class="tps-my-account-avatar"><?php echo get_avatar( $user_id, 150 );?></div>
                    </div>



                    <?php if ( $wishlist_products ) :?>
                    <div class="tps-wishlist-share-container tps-section text-center">
                        <?php
                            if ($wishlist_is_public) {
                                echo do_shortcode('[tps_wishlist_share]');
                            }else {
                                echo do_shortcode('[tps_wishlist_publish]');
                            }
                        ?>
                    </div>
                    <?php endif;?>

                    <div class="tps-wishlist-grid tps-section">

                        <div class="tps-wishlist-empty text-center <?php echo ($wishlist_products) ? 'hide' : ''; ?>">
                            <?php _e('Add your favorites products in your wishlist and share it with your friends.','tps-wishlist');?>
                        </div>
                        <?php
                            if ( $wishlist_products ) {
                                echo do_shortcode('[products ids="' . implode(',', $wishlist_products) . '" columns="3"]');
                            }
                        ?>

                    </div>

                </div>
            </div><!-- row -->
		</div><!-- container -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php

get_footer();
