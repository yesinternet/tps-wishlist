<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product = get_the_title() ;

?>

<a href="<?php echo home_url( '/'.TPS_PROFILE_PAGE.'/' );?>" class="tps-wishlist-btn btn signup" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php _e('Sign up & save it in your wishlist.', 'tps-wishlist');?>" data-product="<?php echo $product ;?>">
    <i class="fa fa-heart fa-fw" aria-hidden="true"></i> <span><?php _e('Add to Wishlist', 'tps-wishlist');?></span>
</a>