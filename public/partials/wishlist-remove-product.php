<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product_id = ( get_the_ID() ) ? get_the_ID() : $_POST['product_id'];
$product = ( get_the_title() ) ? get_the_title() : $_POST['product'];

?>
<a href="#" class="tps-wishlist-ajax tps-wishlist-btn btn remove-product" data-product-id="<?php echo $product_id ;?>" data-product="<?php echo $product;?>">
    <i class="fa fa-remove fa-fw tps-wishlist-heart" aria-hidden="true"></i>
    <i class="tps-wishlist-loading hide fa fa-refresh fa-spin fa-fw"></i> <span><?php _e('Remove from Wishlist','tps-wishlist');?></span>
</a>