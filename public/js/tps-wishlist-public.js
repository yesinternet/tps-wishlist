(function( $ ) {
	'use strict';

	 $(function () {

         $( "body" ).on('click', '.tps-wishlist-ajax', function(event){
            
            event.preventDefault();

            var that = $(this);
            var product_id = that.data("product-id");
            var product = that.data("product");
            var action = '';
            var ga_event_action = '';
            var icons = ".tps-wishlist-loading, .tps-wishlist-heart";

            that.popover('hide');

            if(that.hasClass('add-product')){
                action = 'tps_wishlist_add_product';
                ga_event_action = 'add_product';
            }
            if(that.hasClass('remove-product')){
                action = 'tps_wishlist_remove_product';
                ga_event_action = 'remove_product';
            }

            if(action == '') {
                return false;
            }

            //Trigger the event
            that.trigger( "wishlist-ga-event", [ ga_event_action, product ] );
            
            that.find(icons).toggleClass("hide");

            $.post(
                tps_wishlist_ajax_object.ajax_url,
                {
                    'action': action,
                    'product_id': product_id,
                    'product': product
                },
                function ( response ) {

                    if( response == '0' ){
                        console.log('An error occured while trying to add/remove the product from the wishlist.');
                        that.find(icons).toggleClass("hide");
                        return;
                    }

                    if ( ! response.success ){
                        console.log(response.message);
                        that.popover( { content: 'A request error occured. Please try again.' , placement : 'top' , template : '<div class="popover" role="tooltip"><div class="popover-content bg-danger"></div></div>' } );
                        that.popover('show');
                        that.find(icons).toggleClass("hide");
                        return;
                    }

                    // HTML
                    if ( response.html){
                        var elements = '.tps-wishlist-ajax[data-product-id="' + product_id + '"]';
                        $( elements ).replaceWith( response.html );

                        if(action == 'tps_wishlist_add_product') {
                            
                            var popover_wishlist_elm = $('*[data-product-id="'+product_id+'"]');

                            popover_wishlist_elm.popover( { html: true , content: '<a href="/wishlist"><small>View & Share your wishlist with your friends.</small></a>' , placement : 'top' , template : '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>' } );
                           
                            popover_wishlist_elm.popover('show');

                            setTimeout(function() { popover_wishlist_elm.popover('hide'); }, 3000);        
                        }

                        if(action == 'tps_wishlist_remove_product') {
                            $('.wishlist-wrapper').find(elements).closest('.product.post-' + product_id).fadeOut(500, function(){
                            	$(this).remove();
                            	if($('.tps-wishlist-grid').find('.product.type-product').length <= 0){
                                    $('.tps-wishlist-empty, .tps-wishlist-share-container').toggleClass('hide');
                                }
                            });
                        }
                    }
                });
        });

	 	$( ".tps-wishlist-ajax.publish" ).click( function( event  ) {

	 		event.preventDefault();

	 		var that = $(this);

            //Trigger the event
            that.trigger( "wishlist-ga-event", [ 'publish', '' ] );

	 		that.find(".tps-wishlist-loading").toggleClass("hide");

	 		$.post(
	 			tps_wishlist_ajax_object.ajax_url,
	 			{
	            	'action': 'tps_wishlist_publish'
	        	},
	        	function ( response ) {

	        		if( response == '0' ){
	        			// console.log('An error occured while trying to publish your wishlist.');
	        			that.find(".tps-wishlist-loading").toggleClass("hide");
	        			return;
	        		}

	        		if ( ! response.success ){
	        			// console.log(response.message);
	        			return;
	        		}
	        			// console.log(response);
	        		// HTML
        			if (response.html){
        				that.parent().html( response.html );
        			}

				}
			);


		});

        $( ".tps-wishlist-btn.signup" ).click( function( event  ) {
           
            var that = $(this);
            var product = that.data("product");

            //Trigger the event
            that.trigger( "wishlist-ga-event", [ 'signup', product ] );
        });

    });

})( jQuery );
