<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Wishlist
 * @subpackage Tps_Wishlist/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tps_Wishlist
 * @subpackage Tps_Wishlist/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Wishlist_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
