<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Wishlist_Shortcodes class
 */

class Tps_Wishlist_Shortcodes {

	/**
	 * Init shortcodes.
	 */
	public static function init() {
		
		$shortcodes = array(
			'tps_wishlist_add_product'   	=> __CLASS__ . '::add_product',
			'tps_wishlist_remove_product'	=> __CLASS__ . '::remove_product',
			'tps_wishlist_publish'			=> __CLASS__ . '::publish',
			'tps_wishlist_share'			=> __CLASS__ . '::share',
			'tps_wishlist_login_link'		=> __CLASS__ . '::login_link',
		);

		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( $shortcode, $function );
		}

	}

	/**
	 * Add Product to Wishlist link shortcode
	 */
	public static function add_product( $atts ) {
		
		$atts = shortcode_atts( array(
			//'sample_attr' => '12',
		), $atts, 'tps_wishlist_add_product' );

		ob_start();

		include ( TPS_WISHLIST_TEMPLATES_PATH . '/wishlist-add-product.php');

		$html =  ob_get_clean();

		return $html;
	}

	/**
	 * Remove Product to Wishlist link shortcode
	 */
	public static function remove_product( $atts ) {
		
		$atts = shortcode_atts( array(
			//'sample_attr' => '12',
		), $atts, 'tps_wishlist_remove_product' );

		ob_start();

		include ( TPS_WISHLIST_TEMPLATES_PATH . '/wishlist-remove-product.php');

		$html =  ob_get_clean();

		return $html;
	}

	/**
	 * Publish Wishlist link shortcode
	 */
	public static function publish( $atts ) {
		
		$atts = shortcode_atts( array(
			//'sample_attr' => '12',
		), $atts, 'tps_wishlist_publish' );

		ob_start();

		include ( TPS_WISHLIST_TEMPLATES_PATH . '/wishlist-publish.php');

		$html =  ob_get_clean();

		return $html;
	}

    /**
     * Share Wishlist link shortcode
     */
    public static function share( $atts ) {

        $atts = shortcode_atts( array(
            //'sample_attr' => '12',
        ), $atts, 'tps_wishlist_share' );

        ob_start();

        include ( TPS_WISHLIST_TEMPLATES_PATH . '/wishlist-share.php');

        $html =  ob_get_clean();

        return $html;
    }

	/**
	 * Wishlist login link shortcode
	 */
	public static function login_link( $atts ) {
		
		$atts = shortcode_atts( array(
			//'sample_attr' => '12',
		), $atts, 'tps_wishlist_login_link' );

		ob_start();

		include ( TPS_WISHLIST_TEMPLATES_PATH . '/wishlist-login-link.php');

		$html =  ob_get_clean();

		return $html;
	}

}
