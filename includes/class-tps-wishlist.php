<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Wishlist
 * @subpackage Tps_Wishlist/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Wishlist
 * @subpackage Tps_Wishlist/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Wishlist {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Wishlist_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->define_constants();
		$this->load_config();

		$this->plugin_name = 'tps-wishlist';
		$this->version = '1.0.4';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		
		$this->shortcodes_init();


	}

	/**
	 * Define Constants.
	 */
	private function define_constants() {
		
		$this->define( 'TPS_WISHLIST_PLUGIN_PATH', WP_PLUGIN_DIR.'/tps-wishlist' );
		$this->define( 'TPS_WISHLIST_TEMPLATES_PATH', TPS_WISHLIST_PLUGIN_PATH.'/public/partials' );
		//$this->define( 'TPS_NEWSLETTER_VERSION', $this->version );

	}

	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Load config file
	 */
	private function load_config() {

		if ( is_file( TPS_WISHLIST_PLUGIN_PATH . '/config.php' ) )
		
			require_once TPS_WISHLIST_PLUGIN_PATH . '/config.php';

	}

	/**
	 * Init shortcodes
	 */
	private function shortcodes_init() {

		Tps_Wishlist_Shortcodes::init();

	}



	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Wishlist_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Wishlist_i18n. Defines internationalization functionality.
	 * - Tps_Wishlist_Admin. Defines all hooks for the admin area.
	 * - Tps_Wishlist_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-wishlist-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-wishlist-public.php';

		$this->loader = new Tps_Wishlist_Loader();

		/**
		* The class responsible for wishlist user meta
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/user-meta/class-tps-user-meta-wishlist.php';

		/**
		* The class responsible for html
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-html.php';

		/**
		* The class responsible for ajax functionality.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-ajax.php';

		/**
		 * The class responsible for registering wishlist endpoints
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-endpoints.php';

		/**
		 * The class responsible for wishlist shortcodes
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-shortcodes.php';

		/**
		 * The class responsible for wishlist helper class methods and functions
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-wishlist-helpers.php';

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Wishlist_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Wishlist_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Wishlist_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Wishlist_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		//Add wishlist endpoints
		$this->loader->add_action( 'init', 'Tps_Wishlist_Endpoints' , 'rewrite_rules' ) ;

		$this->loader->add_filter( 'query_vars', 'Tps_Wishlist_Endpoints', 'query_vars' ) ;
		$this->loader->add_filter( 'template_include', 'Tps_Wishlist_Endpoints', 'template_include' , 15 ) ;
		$this->loader->add_filter( 'wpseo_title', 'Tps_Wishlist_Endpoints', 'wp_title' ) ;
		$this->loader->add_filter( 'wpseo_breadcrumb_single_link_info', 'Tps_Wishlist_Endpoints', 'breadcrumb_title' ) ;

		//If user does not exist or wishlist is private, send a 404 page
		$this->loader->add_action( 'template_redirect', 'Tps_Wishlist_Endpoints', 'wishlist_view_route' );

		//Prevent email endpoints from index
		$this->loader->add_action( 'template_redirect', 'Tps_Wishlist_Endpoints', 'no_index' );

		//Single product button
		$this->loader->add_action( 'tps-single-product-wishlist-button', 'Tps_Wishlist_Html', 'add_remove_product' );
		//Grid button
		$this->loader->add_action( 'tps-product-wishlist-button', 'Tps_Wishlist_Html', 'add_remove_product' );
        $this->loader->add_filter( 'woocommerce_account_menu_items', 'Tps_Wishlist_Html', 'add_wishlist_account_menu_item', 10, 1 ) ;
        $this->loader->add_filter( 'wp_nav_menu_objects', 'Tps_Wishlist_Html', 'add_wishlist_top_menu_item' ) ;

		$this->loader->add_action( 'wp_ajax_tps_wishlist_add_product', 'Tps_Wishlist_Ajax', 'add_product' );
		$this->loader->add_action( 'wp_ajax_tps_wishlist_remove_product', 'Tps_Wishlist_Ajax', 'remove_product' );
		$this->loader->add_action( 'wp_ajax_tps_wishlist_publish', 'Tps_Wishlist_Ajax', 'publish' );

		//Show and save wishlist user meta
		$this->loader->add_action( 'show_user_profile', 'Tps_User_Meta_Wishlist', 'render_backend' );
		$this->loader->add_action( 'edit_user_profile', 'Tps_User_Meta_Wishlist', 'render_backend' );
		$this->loader->add_action( 'woocommerce_edit_account_form', 'Tps_User_Meta_Wishlist', 'render_frontend' );
		
		$this->loader->add_action( 'personal_options_update', 'Tps_User_Meta_Wishlist', 'save' );
		$this->loader->add_action( 'edit_user_profile_update', 'Tps_User_Meta_Wishlist', 'save' );
		$this->loader->add_action( 'woocommerce_save_account_details', 'Tps_User_Meta_Wishlist', 'save' );

		// Email automation
		// Add the emails endpoint
		$this->loader->add_filter( 'tps_newsletter_emails_endpoints', 'Tps_Wishlist_Endpoints', 'emails_endpoint' ) ;
		// Return the template
		$this->loader->add_filter( 'tps_newsletter_email_template', 'Tps_Wishlist_Endpoints', 'emails_template' ) ;
		// Redirect page if $_GET['user_email'] is set
		$this->loader->add_action( 'template_redirect', 'Tps_Wishlist_Endpoints', 'check_user_email' );
		// Set user wishlist email title
        $this->loader->add_filter( 'tps_newsletter_email_title', 'Tps_Wishlist_Endpoints', 'emails_title' ) ;

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Wishlist_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
