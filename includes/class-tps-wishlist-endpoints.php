<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Wishlist_Endpoints class
 */
class Tps_Wishlist_Endpoints {

	protected static $wishlist_endpoint = TPS_WISHLIST_ENDPOINT ;
	
	/**
	 * Rewrite rules.
	 */
	public static function rewrite_rules() {

		$wishlist_endpoint = self::$wishlist_endpoint;

		add_rewrite_rule(
			'^'.$wishlist_endpoint.'/([0-9]+)?',
			'index.php?'.$wishlist_endpoint.'=$matches[1]',
			'top'
		);

        add_rewrite_endpoint($wishlist_endpoint, EP_ROOT | EP_PAGES);

	}

	/**
	 * Query vars
	 */
	public static function query_vars( $vars ) {

		$wishlist_endpoint = self::$wishlist_endpoint;
		
		$vars[] = $wishlist_endpoint;

		return $vars;
	}

	public static function template_include( $template ) {

		$wishlist_endpoint = self::$wishlist_endpoint ;

		$wishlist_endpoint_query_var = $user_id = get_query_var( $wishlist_endpoint , false );

 		if( $wishlist_endpoint_query_var !== false ) {

 			$wishlist_view_template_path = tps_wishlist_get_template_path('wishlist-view.php');
 			$wishlist_view_login_template_path = tps_wishlist_get_template_path('wishlist-view-login.php');

 			/*
 			* Wishlist DOES NOT exist
 			*/
 			if ( $user_id !='' && ! tps_wishlist_exists ( $user_id ) ) {
 				return get_404_template();
 			}

 			/*
 			* Wishlist is public
 			*/
 			if ( tps_wishlist_is_public( $user_id ) && $wishlist_view_template_path != false ) {

 				return $wishlist_view_template_path;
 			}

 			/* 
 			 * User is NOT logged
 			*/
 			if ( !is_user_logged_in() && $wishlist_view_login_template_path != false ) {
				
				return $wishlist_view_login_template_path;
	 		}

	 	 	if ( $wishlist_view_template_path != false ) {
				
				return $wishlist_view_template_path;
	 		}	
 					
		}

		return $template;

	}

	/**
	 * Prevent private wishlist or non existent user from showing
	 */
	public static function wishlist_view_route() {
		
		global $wp_query;

		$wishlist_endpoint = self::$wishlist_endpoint;

		if ( isset( $wp_query->query_vars[$wishlist_endpoint] ) ){

			/*
			* User is logged in
			*/
			if ( is_user_logged_in() ){

				$current_user = wp_get_current_user(); 
				$wishlist_user_id = get_query_var( $wishlist_endpoint , false );

				if ( $current_user->ID != $wishlist_user_id && !tps_wishlist_is_public( $wishlist_user_id ) ){
					wp_redirect( tps_wishlist_permalink( $current_user->ID ) );
					exit();
				}
			}
		}
	}	

	//Wishlist page title
	public static function wp_title( $title ) {
		
		$wishlist_endpoint = self::$wishlist_endpoint;

	    if( get_query_var( $wishlist_endpoint , false ) !== false ) {

	    	$title = __( 'My wishlist' , 'tps-wishlist' );
	    }

   		return $title;
	}

	//Wishlist breadcrumb title
	public static function breadcrumb_title( $text ) {
		
	    if( get_query_var( self::$wishlist_endpoint , false ) !== false ) {
	    	$text = __('Wishlist','tps-wishlist');
	    }

   		return $text;
	}

	/**
	 * Prevent indexing wishlist page.
	 */
	public static function no_index() 
	{
		global $wp_query;

		$wishlist_endpoint = self::$wishlist_endpoint;

		if ( isset( $wp_query->query_vars[$wishlist_endpoint] ) )
		{
			@header( 'X-Robots-Tag: noindex' );
		}

	}

	/*
	* Email automation (tps-newsletter)
	*/

	// Add the emails endpoint for user wishlist
	public static function emails_endpoint( $emails_endpoints ) {

		$emails_endpoints[] = TPS_WISHLIST_EMAILS_ENDPOINT;

		return $emails_endpoints;

	}
	
	// Load the user_wishlist.php template user wishlist emails
	public static function emails_template( $template ) {

		if( get_query_var( TPS_WISHLIST_EMAILS_ENDPOINT , false ) !== false ){

			$template = tps_wishlist_get_template_path('emails/endpoints/user_wishlist.php');
		}

		return $template;
	}

	// Check user email URL parameter
	public static function check_user_email() {
		
		global $wp_query;

		if ( isset( $wp_query->query_vars[TPS_WISHLIST_EMAILS_ENDPOINT] ) ){

			/*
			$access_key = ( isset ( $_GET['access_key'] ) ) ? urldecode ( $_GET['access_key'] ) : null ;

			if( $access_key != TPS_WISHLIST_EMAILS_ENDPOINT_ACCESS_KEY ){

				$wp_query->set_404();
				status_header( 404 );
				nocache_headers();
				include( get_query_template( '404' ) );
        		exit(); 
			}
			*/

			$user_email = ( isset ( $_GET['user_email'] ) ) ? urldecode ( $_GET['user_email'] ) : null ;
			
			if( empty ($user_email) ){

				$wp_query->set_404();
				status_header( 404 );
				nocache_headers();
				include( get_query_template( '404' ) );
        		exit(); 
			}

			$user = get_user_by( 'email', $user_email  );

			if ( !$user ) {

				$wp_query->set_404();
				status_header( 404 );
				nocache_headers();
				include( get_query_template( '404' ) );
        		exit(); 
			}

		}

	}

	// Set user wishlist email title
	public static function emails_title($title){
		
		global $wp_query;

	    if ( isset ( $wp_query->query_vars[ TPS_WISHLIST_EMAILS_ENDPOINT ] ) ){
	    	
	    	$title = __('Share your Wishlist with your friends' , 'tps-wishlist');
	    }		

	    return $title;
	}

}