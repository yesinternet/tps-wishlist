<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Wishlist_Helpers class
 */

class Tps_Wishlist_Helpers {

	public static function wishlist_permalink( $user_id = null ){

		if ( !$user_id ){
	    	$user = wp_get_current_user();
	    	$user_id = $user->ID;
		}

		if ( !$user_id ){
			return false;
		}
		
		return get_home_url( null, '/'.TPS_WISHLIST_ENDPOINT.'/'.$user_id.'/' );

	}

	public static function wishlist_products( $user_id = null ){

		if ( !$user_id ){
	    	$user = wp_get_current_user();
	    	$user_id = $user->ID;
		}

		if ( !$user_id ){
			return false;
		}

		$wishlist_products = get_user_meta( $user_id, '_tps_wishlist', true );

		if ( empty ($wishlist_products) ){
			return false;
		}
		
		return $wishlist_products;

	}

	public static function is_product_in_wishlist ( $product_id = null , $wishlist_products = array() ){

		if ( !$product_id || empty ( $wishlist_products ) ){
			return false;
		}

		if ( in_array( $product_id , $wishlist_products ) ){
			return true;
		}
		
		return false;
	}

	public static function add_product_to_wishlist ( $product_id = null , $user_id = null){

    	$user_wishlist_product_ids = get_user_meta( $user_id, '_tps_wishlist', true );

    	if ( empty ( $user_wishlist_product_ids )){
    		$user_wishlist_product_ids = array();
    	}

    	if ( in_array ( $product_id , $user_wishlist_product_ids ) ){
    		return true;
    	}

    	$user_wishlist_product_ids[] = $product_id ;

    	$user_wishlist_product_ids = array_unique( $user_wishlist_product_ids );

    	$add_product_to_wishlist = update_user_meta( $user_id, '_tps_wishlist' ,  $user_wishlist_product_ids );

    	$update_wishlist_updated_meta = update_user_meta( $user_id, '_tps_wishlist_updated' ,  time() );

    	do_action( 'tps_wishlist_after_add_product', $add_product_to_wishlist , $product_id , $user_id , $user_wishlist_product_ids);

    	/*
    	 * Update field_wishlist_updated EE custom field
    	 */
		$user = get_userdata( $user_id );

		if( class_exists('Tps_Newsletter_EE_API') ) {
            $ee_contact_update = Tps_Newsletter_EE_API::contact_update($user->user_email, array('field_wishlist_updated' => Tps_Newsletter_EE_API::get_formatted_timestamp(time())));

            //if ( $ee_contact_update['success'] == false ){

            //	return false;
            //}
        }

    	return $add_product_to_wishlist;

	}

	public static function remove_product_from_wishlist ( $product_id = null , $user_id = null){

    	$user_wishlist_product_ids = get_user_meta( $user_id, '_tps_wishlist', true );

    	if ( empty ( $user_wishlist_product_ids ) ){
    		$user_wishlist_product_ids = array();
    	}

    	if ( !in_array ( $product_id , $user_wishlist_product_ids ) ){
    		return true;
    	}

    	$user_wishlist_product_ids = array_diff( $user_wishlist_product_ids , array( $product_id ));

       	$remove_product_from_wishlist = update_user_meta( $user_id, '_tps_wishlist' ,  $user_wishlist_product_ids );

    	$update_wishlist_updated_meta = update_user_meta( $user_id, '_tps_wishlist_updated' ,  time() );
    	
    	do_action( 'tps_wishlist_after_remove_product', $remove_product_from_wishlist , $product_id , $user_id , $user_wishlist_product_ids);

    	/*
    	 * Update field_wishlist_updated EE custom field
    	 */

    	// If the wishlist is empty after removal, then updated time is null
    	$time = ( ! empty( $user_wishlist_product_ids ) ) ? time() : null ;
		$user = get_userdata( $user_id );

        if( class_exists('Tps_Newsletter_EE_API') ) {
            $ee_contact_update = Tps_Newsletter_EE_API::contact_update($user->user_email, array('field_wishlist_updated' => Tps_Newsletter_EE_API::get_formatted_timestamp($time)));

            //if ( $ee_contact_update['success'] == false ){

            //return false;
            //}
        }

    	return $remove_product_from_wishlist;

	}	

	public static function wishlist_exists( $user_id = null ){

		// Check if user exists by ID
		$user = get_user_by( 'id', $user_id );

		return ( $user ) ? true : false ;
	}

	public static function wishlist_is_public( $user_id = null ){

		$_tps_wishlist_public = get_user_meta( $user_id, '_tps_wishlist_public', true );

		return ( $_tps_wishlist_public ) ? true : false ;
	}

	public static function get_template_path( $template = '' ){

		//Check theme directory first
		$template_path = locate_template( array( 'tps-wishlist/'.$template ) );

		if( '' != $template_path ){
			return $template_path;
 		}
		
		//Check plugin directory next
		$template_path = TPS_WISHLIST_TEMPLATES_PATH . '/'.$template;

		if( file_exists( $template_path ) ){	
			return $template_path;
		}

		return false;
	}

	public static function get_user_wishlist_updated_time( $user_id ){

	    if( ! class_exists('Tps_Newsletter_EE_API') ) {
	        return '';
        }

		$user_wishlist_updated_time = Tps_Newsletter_EE_API::get_formatted_timestamp() ;

		$wishlist_updated_timestamp = get_user_meta( $user_id, '_tps_wishlist_updated', true);

		if ( !empty($wishlist_updated_timestamp) && !empty ( get_user_meta( $user_id, '_tps_wishlist', true ) ) ){
			
			$user_wishlist_updated_time = Tps_Newsletter_EE_API::get_formatted_timestamp( $wishlist_updated_timestamp ) ;
		}

		return $user_wishlist_updated_time;
	}
}

function tps_wishlist_permalink( $user_id = null ){
	return Tps_Wishlist_Helpers::wishlist_permalink( $user_id );
}

function tps_wishlist_products( $user_id = null ){

	return Tps_Wishlist_Helpers::wishlist_products( $user_id );
}

function tps_wishlist_is_product_in_wishlist( $product_id = null , $wishlist_products = array() ){

	return Tps_Wishlist_Helpers::is_product_in_wishlist( $product_id , $wishlist_products );
}

function tps_wishlist_add_product( $product_id = null , $user_id = null ){

	return Tps_Wishlist_Helpers::add_product_to_wishlist( $product_id , $user_id );
}

function tps_wishlist_remove_product( $product_id = null , $user_id = null ){

	return Tps_Wishlist_Helpers::remove_product_from_wishlist( $product_id , $user_id );
}

function tps_wishlist_exists( $user_id = null ){

	return Tps_Wishlist_Helpers::wishlist_exists( $user_id );
}

function tps_wishlist_is_public( $user_id = null ){

	return Tps_Wishlist_Helpers::wishlist_is_public( $user_id );
}

function tps_wishlist_get_template_path( $template = '' ){

	return Tps_Wishlist_Helpers::get_template_path( $template );
}

function tps_wishlist_get_user_wishlist_updated_time ( $user_id ){

	return Tps_Wishlist_Helpers::get_user_wishlist_updated_time( $user_id );
}