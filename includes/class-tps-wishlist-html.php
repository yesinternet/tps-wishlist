<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tps_Wishlist_Html {

	public static function add_remove_product(){

	    echo '<div class="tps-wishlist-add-remove-container">';
		//Check if user is logged in
		$user = wp_get_current_user();
	    
	    $user_id = $user->ID;

	    //If not logged in, show the login link
	    if ( empty ( $user_id ) ){

			echo do_shortcode ('[tps_wishlist_login_link]');
			echo '</div>';
	    	return;
	    }

	    //User is logged in
	    	    
	    $wishlist_products = tps_wishlist_products( $user_id );

	    $product_id = get_the_ID();
	    
	    $is_product_in_wishlist = tps_wishlist_is_product_in_wishlist( $product_id , $wishlist_products) ;

		if ( $is_product_in_wishlist ) {

            echo do_shortcode('[tps_wishlist_remove_product]');

		}else {

            echo do_shortcode('[tps_wishlist_add_product]');

        }

        echo '</div>';
	}

    public static function add_wishlist_account_menu_item($items) {

	    if(!is_user_logged_in() || !defined('TPS_WISHLIST_ENDPOINT')) return $items;

	    $user_id = get_current_user_id();
	    $endpoint = TPS_WISHLIST_ENDPOINT .'/' . $user_id;

        //Unset logout item in order to reset it to the end of the array.
        $logout_item = $items['customer-logout'];

        unset($items['customer-logout']);

        $items[$endpoint]= __( 'My wishlist' , 'tps-wishlist' );

        $items['customer-logout']= $logout_item;

        return $items;

	}

    public static function add_wishlist_top_menu_item($sorted_menu_items) {

        if(!is_user_logged_in() || !defined('TPS_WISHLIST_ENDPOINT')) return $sorted_menu_items;

	    foreach ($sorted_menu_items as $item){
	        if($item->url == '/' . TPS_WISHLIST_ENDPOINT . '/'){
                $user_id = get_current_user_id();
                $item->url .= $user_id;
            }
        }

        return $sorted_menu_items;
	}

}
