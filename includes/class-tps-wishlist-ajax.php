<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tps_Wishlist_Ajax {

	public static function add_product(){

	    $user = wp_get_current_user();
	    $user_id = $user->ID;

	    if ( empty ( $user_id ) ){
			
			$return = array(

				'success' => false,
				'message' => __('You must login in order to add a product to your wishlist' , 'tps-wishlist')
			);	 

			wp_send_json( $return );   	
	    }

	    // First, we need to make sure the post ID parameter has been set and that it's a numeric value
	    if( isset( $_POST['product_id'] ) && is_numeric( $_POST['product_id'] ) ) {

	    	$product_id = $_POST['product_id'] ;
	    	
	    	$add_product_to_wishlist = tps_wishlist_add_product ( $product_id , $user_id );

	    	if ( !$add_product_to_wishlist )
	    	{

				$return = array(

					'success' => false,
					'message' => __('The product was not added to your wishlist' , 'tps-wishlist')
			
				);

				wp_send_json( $return );

	    	}

	    	if ( $add_product_to_wishlist ){
	    		
	    		ob_start();

				echo do_shortcode('[tps_wishlist_remove_product]');

				$html =  ob_get_clean();

				$return = array(

					'success' => true,
					'data' => null,
					'html' => $html,
					'message' => __('The product has been added to your wishlist' , 'tps-wishlist')
			
				);

				wp_send_json( $return );

	    	}



	    } // end if
	 
	    wp_die();

	} 

	public static function remove_product(){

	    $user = wp_get_current_user();
	    $user_id = $user->ID;
	    
	    if ( empty ( $user_id ) ){
			
			$return = array(

				'success' => false,
				'message' => __('You must login in order to add a product to your wishlist' , 'tps-wishlist')
			);

			wp_send_json( $return );   	
	    }

	    // First, we need to make sure the post ID parameter has been set and that it's a numeric value
	    if( isset( $_POST['product_id'] ) && is_numeric( $_POST['product_id'] ) ) {

	    	$product_id = $_POST['product_id'] ;

	    	$remove_product_from_wishlist = tps_wishlist_remove_product( $product_id , $user_id );

	    	if ( $remove_product_from_wishlist ){
	    		
	    		ob_start();

                echo do_shortcode('[tps_wishlist_add_product]');

				$html =  ob_get_clean();

				$return = array(

					'success' => true,
					'data' => null,
					'html' => $html,
					'message' => __('The product has been removed from your wishlist' , 'tps-wishlist')
			
				);

				wp_send_json( $return );

	    	}

	    	if ( !$remove_product_from_wishlist )
	    	{

				$return = array(

					'success' => false,
					'message' => __('The product was not removed from your wishlist' , 'tps-wishlist')
			
				);

				wp_send_json( $return );

	    	}

	    	
	        
	    } // end if
	 
	    wp_die();

	}

	public static function publish(){

		$current_user = wp_get_current_user();
		
		$user_id = $current_user->ID ;
		
		if ( $user_id != 0 ){
			$update_user_meta = update_user_meta( $user_id, '_tps_wishlist_public', 1 );
			do_action( 'tps_wishlist_after_wishlist_public', $user_id );
			
		}
		


		ob_start();

        echo do_shortcode('[tps_wishlist_share]');

		$html =  ob_get_clean();

		$return = array(

			'success' => true,
			'data' => null,
			'html' => $html,
			'message' => __('Wishlist has been published' , 'tps-wishlist')
		
		);

		wp_send_json($return);
	} 

}
