<?php
/**
 * Wishlist User Meta
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_User_Meta_Wishlist Class.
 */
class Tps_User_Meta_Wishlist {

	/**
	* The HTML for the user meta fields
	*
	*/
	public static function render_backend( $user ) {

		$wishlist_public = get_user_meta( $user->ID , '_tps_wishlist_public', true  ) ;

	?>
	
	<h3><?php _e( 'Wihslist settings' , 'tps-wishlist' );?></h3>

	<table class="form-table">

		<tr>
			<th scope="row"><?php _e( 'Wishlist visibility' , 'tps-wishlist' );?></th>
			<td>
				<fieldset>
					<legend class="screen-reader-text"><span><?php _e( 'Wishlist visibility' , 'tps-wishlist' );?></span></legend>
						<label for="_tps_wishlist_public">
							<input name="_tps_wishlist_public" id="_tps_wishlist_public" value="1" <?php if( $wishlist_public =='1') echo 'checked="checked"';?> type="checkbox">
							<?php _e( 'Check to make the wishlist public and sharable' , 'tps-wishlist' );?>
						</label>
				</fieldset>
			</td>
		</tr>

	</table>
	
	<?php 

	}

	public static function render_frontend( ) {

		$user = wp_get_current_user();
		$wishlist_public = get_user_meta( $user->ID , '_tps_wishlist_public', true );

	?>

	<fieldset>
		<legend><?php _e( 'Wishlist visibility', 'woocommerce' ); ?></legend>

        <div class="form-group">
            <div class="checkbox-inline">
                <label>
                	<input type="checkbox" class="woocommerce-Input" name="_tps_wishlist_public" id="_tps_wishlist_public" value="1" <?php if( $wishlist_public =='1') echo 'checked="checked"';?>>
					<span><?php _e( 'Check to make the wishlist public and sharable' , 'tps-wishlist' );?></span>
                </label>
            </div>
        </div>

	</fieldset>
	<div class="clear"></div>

	
	<?php 

	}

	public static function save( $user_id  ) {
		
		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		if ( isset ( $_POST['_tps_wishlist_public'] ) ){
			update_user_meta( $user_id, '_tps_wishlist_public', $_POST['_tps_wishlist_public'] );
		}

		if ( !isset ( $_POST['_tps_wishlist_public'] ) ){
			delete_user_meta( $user_id, '_tps_wishlist_public' );
		}		

		//$user = wp_update_user( array( 'ID' => $user_id ) );
	}
}