#Description
The Wishlist addon implements a simple wishlist funcionality for the eshop.
* Wishlist works only for logged in users
* Users can add/remove any product in their wishlist via AJAX links
* There are available shortcodes in order to add those links in any place. Shortcodes use loop functions.
* Wishlist products are stored in _tps_wishlist user meta
* For each user, there is a dedicated page for their wishlist, i.e. /wishlist/[user_id]
* There is a user meta that users can make their wishlist public or private
* They can update their preference via their profile edit page and directly on their wishlist page
* Public wishlists can be accessed by anyone and are sharable via Social Media and email

#For developers
##Shortcodes
The following shortcodes are available
* [tps_wishlist_add_product] : anchor link to add product to wishlist via ajax. Inserted inside loop.
* [tps_wishlist_remove_product] : anchor link to add product to wishlist via ajax. Inserted inside loop.
* [tps_wishlist_publish] : anchor link to publish wishlist. used inside wishlist page.
* [tps_wishlist_login_link] : anchor link to redirect to login page, when user is not logged in
##Helper functions
All helper functions are inside Tps_Wishlist_Helpers Class. There are function wrappers for every helper class method.
* tps_wishlist_permalink( $user_id = null )
* tps_wishlist_products( $user_id = null )
* tps_wishlist_is_product_in_wishlist( $product_id = null , $wishlist_products = array() )
* tps_wishlist_add_product( $product_id = null , $user_id = null )
* tps_wishlist_remove_product( $product_id = null , $user_id = null )

# TO DO
* Test again all functionality
* Ensure code security for unpreveldged usage