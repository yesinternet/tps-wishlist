<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


define( 'TPS_WISHLIST_ENDPOINT', 'wishlist' );
define( 'TPS_WISHLIST_EMAILS_ENDPOINT', 'user_wishlist' );
define( 'TPS_WISHLIST_EMAILS_ENDPOINT_ACCESS_KEY', 'elasticemail' );